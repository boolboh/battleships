package main;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.*;

public class Board {
    private int n; // TODO za mało deskryptywna nazwa zmiennej
    private Square[][] board;
    private List<Ship> ships = new ArrayList<>();

    public Board(int n) {
        this.n = n;
        board = new Square[n][n];
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                board[i][j] = new Square();
            }
        }
    }
	
	// FIXME rozbić na osobne metody, tam gdzie komentarz to dobre miejsce na rozbicie
    public Boolean validateShip(Coords beg, Coords end, int len) {
        // validate constrains
        if(beg.getX() > n - 1 || beg.getX() < 0) return false;
        if(beg.getY() > n - 1 || beg.getY() < 0) return false;
        if(end.getX() > n - 1 || end.getX() < 0) return false;
        if(end.getY() > n - 1 || end.getY() < 0) return false;

        //validate being in one line
        if(beg.getX() != end.getX() && beg.getY() != end.getY()) return false;

        // validate other ship placement and length
        if(beg.getX() == end.getX()) {
            if(abs(beg.getY() - end.getY()) != len - 1) return false;
            for(int i = min(beg.getY(), end.getY()); i <= max(beg.getY(), end.getY()); i++) {
                if(board[beg.getX()][i].hasShip()) return false;
            }
        } else {
            if(abs(beg.getX() - end.getX()) != len - 1) return false;
            for(int i = min(beg.getX(), end.getX()); i <= max(beg.getX(), end.getX()); i++) {
                if(board[i][beg.getY()].hasShip()) return false;
            }
        }

        return true;
    }

    // Assumes ship is validated.
    public void addShip(Coords beg, Coords end, int len) {
        Ship ship = new Ship(len);
        ships.add(ship);
        int line, begShip, endShip;
        if(beg.getX() == end.getX()) {
            begShip = min(beg.getY(), end.getY());
            endShip = max(beg.getY(), end.getY());
            for(int i = begShip; i <= endShip; i++) {
                board[beg.getX()][i].setShip(ship);
            }
        } else {
            line = beg.getY();
            begShip = min(beg.getX(), end.getX());
            endShip = max(beg.getX(), end.getX());
            for(int i = begShip; i <= endShip; i++) {
                board[i][beg.getY()].setShip(ship);
            }
        }
    }

    public Boolean allShipsSunk() {
        Boolean sunk = true;
        for(Ship s: ships)
            sunk &=  s.isSunk();
        return sunk;
    }
    public Square getSquare(Coords c) {
        return board[c.getX()][c.getY()];
    }
    //FIXME get
    public int n() {
        return n;
    }
}
