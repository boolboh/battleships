package main;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Coords {
    int x, y;


    public Coords(String s) throws IllegalArgumentException{
        String[] coords = s.split("\\D+");
        if(coords.length != 2) throw new IllegalArgumentException();
        // TODO tu trzeba sprawdzić, czy na pewno to jest int
        this.x = Integer.parseInt(coords[0]);
        this.y = Integer.parseInt(coords[1]);
    }

    public Coords(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public int getX() {
        return x;
    }

    // FIXME settery powinny mieć podstawową walidację (np. sprawdzenie, czy x nie jest ujemny albo poza planszą
    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }
    // FIXME settery powinny mieć podstawową walidację (np. sprawdzenie, czy y nie jest ujemny albo poza planszą
    public void setY(int y) {
        this.y = y;
    }
}
