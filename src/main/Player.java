package main;

public class Player {
    //TODO czy ta zmienna jest gdziekolwiek używana? Jeśli zbędna, usunąć.
    private String name;
    private Board board;

    public Player(String name) {
        this.name = name;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }
    //TODO 'hasLost' zamiast 'ifLost' - konwencja
    public Boolean ifLost() { return board.allShipsSunk(); }
}
