package main;

public class Square {
    private Boolean hit = false;
    private Ship ship = null;

    public Boolean isHit() {
        return hit;
    }

    public Boolean hasShip() {
        return ship != null;
    }

    public void hit() {
        this.hit = true;
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }
}
