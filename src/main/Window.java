package main;

import java.util.Scanner;

public class Window {
    private final char WATER = '░';
    private final char SHIP = '■';
    private final char WATER_HIT = '▓';
    private final char HIT = 'X';
    //TODO MAX_WIDTH prawie wszędzie jest używane z /2 -2, może warto dać dwie stałe?
    private final int MAX_WIDTH = 183;

    private String[] boards;
//    private String messageLine;
    //TODO czym jest n?
    private int n;

    //TODO czym jest N? Ta metoda ma mało deskryptywną nazwę.
    public int initN() {
        Scanner scanner = new Scanner(System.in);
        printPlayerTexts("Podaj wielkość planszy do gry:", "Podaj wielkość planszy do gry:\n");
        while (!scanner.hasNextInt()) scanner.next();
        this.n = scanner.nextInt();

        boards = new String[lastLine()];

        cls();
        clearBoards();
        clearMessage();

        return this.n;
    }

    public String readLine(GameData gd) {
        if (!gd.isFirstPlayer(gd.getActivePlayer())) {
            printPlayerTexts("", "");
        }
        return new Scanner(System.in).nextLine();
    }

    public void buildData(GameData gd) {
        clearBoards();

        buildBoard(gd, gd.getPlayers()[0], false);
        buildBoard(gd, gd.getPlayers()[0], true);

        addBorder();

        buildBoard(gd, gd.getPlayers()[1], false);
        buildBoard(gd, gd.getPlayers()[1], true);
    }

    //TODO magic numbers. Użyte raz -> komentarz dlaczego taka wartość. więcej niż raz -> stała.
    private void buildBoard(GameData gd, Player player, Boolean isOpponentsBoard) {
        int start_row = isOpponentsBoard ? 0 : n + 5;
        addCoords(start_row);
        addLine(start_row + 2);
        addBoard(gd, player, isOpponentsBoard, start_row + 3);
        addLine(start_row + n + 3);
    }

    private void clearBoards() {
        for (int i = 0; i < lastLine() - 1; i++)
            boards[i] = "";
    }

    public void clearMessage() {
        boards[lastLine() - 1] = " ".repeat(MAX_WIDTH / 2 - 2) + "III" + " ".repeat(MAX_WIDTH / 2 - 2);
    }

    //    public void addText(String text)
    //FIXME powtarzający się kod. Wyeksportować metodę i dodać parametry.
    private void addCoords(int start_index) {
        // first row
        StringBuilder sb = new StringBuilder();
        sb.append("    ");
        for (int i = 0; i < n; i++)
            sb.append(" ").append(((i / 10 == 0) ? " " : (i / 10)));
        sb.append("  ");
        boards[start_index] += sb.toString();

        // second row
        sb = new StringBuilder();
        sb.append("    ");
        for (int i = 0; i < n; i++)
            sb.append(" ").append(i % 10);
        sb.append("  ");
        boards[start_index + 1] += sb.toString();

        //first and second column
        for (int i = 0; i < n; i++)
            boards[start_index + i + 3] += (i < 10) ? (" " + i + " ") : (i + " ");
    }

    private void addBoard(GameData gd, Player player, Boolean isOpponentsBoard, int start_index) {
        Board board = isOpponentsBoard ? gd.getOtherPlayer(player).getBoard() : player.getBoard();
        Square square;
        for (int i = 0; i < n; i++) {
            boards[start_index + i] += "|";
            for (int j = 0; j < n; j++) {
                square = board.getSquare(new Coords(i, j));
                if (square.isHit())
                    if(square.hasShip())
                        boards[start_index + i] += " " + HIT;
                    else
                        boards[start_index + i] += " " + WATER_HIT;

                else {
                    if (square.hasShip()) {
                        boards[start_index + i] += " ";
                        boards[start_index + i] += (isOpponentsBoard ? WATER : SHIP);
                    } else
                        boards[start_index + i] += " " + WATER;
                }
            }
            boards[start_index + i] += " |";
        }
    }

    private void addLine(int line) {
        boards[line] += "    " + "-".repeat(2 * n + 1) + " ";
    }

    private void addBorder() {
        for (int i = 0; i < lastLine() - 1; i++) {
            int spaces_to_border = (MAX_WIDTH / 2) - 2 - boards[i].length();
            boards[i] += " ".repeat(spaces_to_border) + "III" + " ".repeat(spaces_to_border);
        }
    }

//    public void addMessageActivePlayer(GameData gd, String text) {
//        boards[lastLine() - 1] = gd.isFirstPlayer(gd.getActivePlayer()) ?
//                generatePlayerTexts(text, "") : generatePlayerTexts("", text);
//    }
    //TODO magic number
    private int lastLine() {
        return 2 * n + 12;
    }

    private String generatePlayerTexts(String text1, String text2) {
        //TODO magic number
        return text1 + " ".repeat((MAX_WIDTH / 2) - 2 - text1.length()) + "III " + " ".repeat((MAX_WIDTH / 8)) + text2;
    }

    private void printPlayerTexts(String text1, String text2) {
        System.out.print(generatePlayerTexts(text1, text2));
    }

    public void printTextActivePlayer(GameData gd, String playerText, String otherPlayerText) {
        System.out.print(gd.isFirstPlayer(gd.getActivePlayer()) ?
                generatePlayerTexts(playerText, otherPlayerText + "\n") :
                generatePlayerTexts(otherPlayerText, playerText + "\n"));
    }

    public void printTextActivePlayer(GameData gd, String playerText) {
        printTextActivePlayer(gd, playerText, "CZEKAJ NA SWOJĄ KOLEJ");
    }

    //FIXME są funkcje systemowe, które czyszczą ekran, czy to na windowsie czy ubuntu
    private void cls() {
        System.out.println("\n".repeat(100));
    }

    public void print() {
        cls();
        for (int i = 0; i < lastLine(); i++) {
            System.out.println(boards[i]);
        }
    }
}
