package test;
import main.Board;
import main.Coords;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BoardTest {
	// TODO co właściwie sprawdzają te testy? Nazwy testów są za mało deskryptywne.
    @Test
    public void validateShipTest1() {
        Board board = new Board(11);
        Assertions.assertFalse(board.validateShip(new Coords("10 9"), new Coords("10 8"), 3));
    }
    @Test
    public void validateShipTest2() {
        Board board = new Board(5);
        Assertions.assertFalse(board.validateShip(new Coords("10 10"), new Coords("10 11"), 1));
    }
    @Test
    public void validateShipTest3() {
        Board board = new Board(3);
        Assertions.assertFalse(board.validateShip(new Coords("10 11"), new Coords("10 10"), 1));
    }
    @Test
    public void validateShipTest4() {
        Board board = new Board(11);
        Assertions.assertTrue(board.validateShip(new Coords("10 9"), new Coords("10 8"), 2));
    }
}